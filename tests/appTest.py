import unittest
from app import app

class AppTest(unittest.TestCase):
    def setUp(self):
        self.app = app.test_client()

    def test_http_code(self):
        response = self.app.get('/')
        self.assertEqual(200, response.status_code, "Deu ruim no test_http_code!")

    def test_menu(self):
        response = self.app.get('/')
        conteudo = response.get_data(as_text= True)
        home_exists = conteudo.__contains__('<a class="nav-item nav-link" id="home-menu" href="#">Home</span></a>')
        self.assertTrue(home_exists,"Menu home não encontrado")
        agencia_exists = conteudo.__contains__('<a class="nav-item nav-link" id="about-menu" href="#">A agência</a>')
        self.assertTrue(agencia_exists,"Menu agência não encontrado")
        services_exists = conteudo.__contains__('<a class="nav-item nav-link" id="services-menu" href="#">Serviços</a>')
        self.assertTrue(services_exists,"Menu serviços não encontrado")
        time_exists = conteudo.__contains__('<a class="nav-item nav-link" id="team-menu" href="#">Time</a>')
        self.assertTrue(time_exists,"Menu time não encontrado")
        projeto_exists = conteudo.__contains__('<a class="nav-item nav-link" id="portfolio-menu" href="#">Projetos</a>')
        self.assertTrue(projeto_exists,"Menu projeto não encontrado")
        contato_exists = conteudo.__contains__('<a class="nav-item nav-link" id="contact-menu" href="#">Contato</a>')
        self.assertTrue(contato_exists,"Menu contato não encontrado")

    def test_carousel(self):
        response = self.app.get('/')
        conteudo = response.get_data(as_text=True)
        h2_exists = conteudo.__contains__('<h2>Quer criar um e-commerce?</h2>')
        self.assertTrue(h2_exists,"Carousel h2 não encontrado")
        p_exists = conteudo.__contains__('<p>Conte conosco, temos mais de 30 lojas no portifólio</p>')
        self.assertTrue(p_exists,"Carousel p não encontrado")

    def sobre_empresa(self):
        response = self.app.get('/')
        conteudo = response.get_data(as_text=True)   
        sobre_exists = conteudo.__contains__('<h3 class="main-title">Sobre a hDC Agency</h3>')
        self.assertTrue(sobre_exists,"sobre não encontrado")
        h3_exists = conteudo.__contains__('<h3 class="about-title">Uma agência que pensa no futuro</h3>')
        self.assertTrue(h3_exists,"h3 não encontrado")
        p1_exists = conteudo.__contains__('<p>Nossos projetos são adaptados ao cliente e seu propósito.</p>')
        self.assertTrue(p1_exists,"p1 não encontrado")
        p2_exists = conteudo.__contains__('<p>Após a especificação do projetos os arquitetos de software definirão as melhores tecnologias para seu projeto.</p>')
        self.assertTrue(p2_exists,"p2 não encontrado")
        p3_exists = conteudo.__contains__('<p>E nossos designers trabalharão na sua interface/layout para impulsionar o negócio.</p>')
        self.assertTrue(p3_exists,"p3 não encontrado")
        p4_exists = conteudo.__contains__('<p>Veja outros diferenciais:</p>')
        self.assertTrue(p4_exists,"p4 não encontrado")
        li_exists = conteudo.__contains__('<li><i class="fas fa-check"></i> Utilização de Machine Learning / AI</li>')
        self.assertTrue(li_exists,"li não encontrado")
        li2_exists = conteudo.__contains__('<li><i class="fas fa-check"></i> Layout responsivo para todos os dispositivos</li>')
        self.assertTrue(li2_exists,"li2 não encontrado")
        li3_exists = conteudo.__contains__('<li><i class="fas fa-check"></i> Integração com diversos sistemas do mercado</li>')
        self.assertTrue(li3_exists,"li3 não encontrado")
        li4_exists = conteudo.__contains__('<li><i class="fas fa-check"></i> Sistema de pagamento próprio</li>')
        self.assertTrue(li4_exists,"li4 não encontrado")
        li5_exists = conteudo.__contains__('<li><i class="fas fa-check"></i> Desenvolvimento com metodologia ágil</li>')
        self.assertTrue(li5_exists,"li5 não encontrado")
        

if __name__ == "__main__":
    unittest.main()
