from flask import Flask, render_template
import unittest
app = Flask(__name__)


class AppTest(unittest.TestCase):
    def setUp(self):
        self.app = app.test_client()


@app.route('/')
def home():
    return render_template("index.html")


@app.route('/<string:nome>')
def error(nome):
    return f'A página ({nome}) não existe'


if __name__ == "__main__":
    app.run()
